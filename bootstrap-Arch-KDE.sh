#!/usr/bin/env bash

MOUNT=/mnt

arch() { arch-chroot "$MOUNT" /bin/bash -c "$1"; }

arch "pacman -S plasma plasma-wayland-session kdeadmin kdebase kdeutils"
arch "systemctl enable NetworkManager.service"
arch "systemctl enable sddm.service"

echo ""
echo "DONE!"
echo ""
echo "Time to reboot."
echo "Nice things to do: install preload, graphics driver!, set up hibernation, snapper, backups, ...."
