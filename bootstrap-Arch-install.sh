#!/usr/bin/env bash

# Set up partitions first! Then enter the required variables.
# If encryption should be used, uncomment the corresponding parts and change BOOTPART to /dev/mapper/crypto
# In order to speed up the download and install, consider to edit
# /etc/pacman.d/mirrorlist before starting!

# Mouse support
systemctl start gpm.service
# Update system clock
timedatectl set-ntp true

# * Helper functions
exists() { command -v "$1" >/dev/null; }
arch() { arch-chroot "$MOUNT" /bin/bash -c "$1"; }
pac() { pacstrap "$MOUNT" $@; }
contains_element() {
    #check if an element exist in a string
    for e in "${@:2}"; do [[ $e == $1 ]] && break; done;
}
read_input_text() {
    read -p "$1 [y/N]: " OPTION
    echo ""
    OPTION=`echo "$OPTION" | tr '[:upper:]' '[:lower:]'`
}
# Mirrorlist
configure_mirrorlist(){
    local countries_code=("AU" "AT" "BY" "BE" "BR" "BG" "CA" "CL" "CN" "CO" "CZ" "DK" "EE" "FI" "FR" "DE" "GR" "HK" "HU" "ID" "IN" "IR" "IE" "IL" "IT" "JP" "KZ" "KR" "LV" "LU" "MK" "NL" "NC" "NZ" "NO" "PL" "PT" "RO" "RU" "RS" "SG" "SK" "ZA" "ES" "LK" "SE" "CH" "TW" "TR" "UA" "GB" "US" "UZ" "VN")
    local countries_name=("Australia" "Austria" "Belarus" "Belgium" "Brazil" "Bulgaria" "Canada" "Chile" "China" "Colombia" "Czech Republic" "Denmark" "Estonia" "Finland" "France" "Germany" "Greece" "Hong Kong" "Hungary" "Indonesia" "India" "Iran" "Ireland" "Israel" "Italy" "Japan" "Kazakhstan" "Korea" "Latvia" "Luxembourg" "Macedonia" "Netherlands" "New Caledonia" "New Zealand" "Norway" "Poland" "Portugal" "Romania" "Russia" "Serbia" "Singapore" "Slovakia" "South Africa" "Spain" "Sri Lanka" "Sweden" "Switzerland" "Taiwan" "Turkey" "Ukraine" "United Kingdom" "United States" "Uzbekistan" "Viet Nam")
    country_list(){
        PS3="$prompt1"
        echo "Select your country:"
        select country_name in "${countries_name[@]}"; do
            if contains_element "$country_name" "${countries_name[@]}"; then
                country_code=${countries_code[$(( $REPLY - 1 ))]}
                break
            else
                echo "Invalid option. Try another one."
            fi
        done
    }
    echo "MIRRORLIST - https://wiki.archlinux.org/index.php/Mirrors"
    echo "This option is a guide to selecting and configuring your mirrors, and a listing of current available mirrors."
    OPTION=n
    while [[ $OPTION != y ]]; do
        country_list
        read_input_text "Confirm country: $country_name"
    done

    url="https://www.archlinux.org/mirrorlist/?country=${country_code}&use_mirror_status=on"

    tmpfile=$(mktemp --suffix=-mirrorlist)

    # Get latest mirror list and save to tmpfile
    curl -so ${tmpfile} ${url}
    sed -i 's/^#Server/Server/g' ${tmpfile}

    # Backup and replace current mirrorlist file (if new file is non-zero)
    if [[ -s ${tmpfile} ]]; then
        { echo " Backing up the original mirrorlist..."
          mv -i /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.orig; } &&
            { echo " Rotating the new list into place..."
              mv -i ${tmpfile} /etc/pacman.d/mirrorlist; }
    else
        echo " Unable to update, could not download list."
    fi
    # better repo should go first
    pacman -Sy pacman-contrib
    cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.tmp
    rankmirrors /etc/pacman.d/mirrorlist.tmp > /etc/pacman.d/mirrorlist
    rm /etc/pacman.d/mirrorlist.tmp
    # allow global read access (required for non-root yaourt execution)
    chmod +r /etc/pacman.d/mirrorlist
    $EDITOR /etc/pacman.d/mirrorlist
}

# * Welcome
echo ""
echo "Time to install Arch Linux"
echo ""
echo "YOU HAVE TO SET UP YOUR PARTITIONS BEFORE CONTINUING!"
echo "YOU HAVE TO USE GPT!"
echo ""
echo "If you want to use logging:"
echo "bash bootstraph.sh 2>&1 | tee LogFile.txt"
echo ""
echo "If we have problems with the keyring, consider aborting and run:"
echo "pacman-key --refresh-keys"
echo "pacman -Syy"
read -n 1 -s -r -p "Press any key to continue or Ctrl-C to abort ..."

# * Variables
partprobe
lsblk -f
read -rp "Boot partition (/dev/sdX): " BOOTPART
# Encryption and Root partition
while true; do
    read -rp "Use encryption? [y/n]: " crypto
    case $crypto in
        [Yy]* )
            read -rp "Partition to encrypt (/dev/sdX): " CRYPTPART
            cryptsetup luksFormat "$CRYPTPART"
            cryptsetup open "$CRYPTPART" crypto
            ROOTPART=/dev/mapper/crypto
            break
            ;;
        [Nn]* )
            read -rp "Root partition (/dev/sdX): " ROOTPART
            break
            ;;
        * ) echo "Please answer yes or no."
            ;;
    esac
done
# Swap
free -g
read -rp "Swap size: " SWAPSIZE
# Hostname
read -rp "Host name: " HOSTNAME
# Microcode
while true; do
    read -rp "Use Intel or AMD microcode? [intel/amd]: " micro
    case $micro in
        [intel]* )
            MICRO=intel-ucode
            break
            ;;
        [amd]* )
            MICRO=amd-ucode
            break
            ;;
        * ) echo "Please answer intel or amd."
            ;;
    esac
done

MOUNT=/mnt

# * Format and mount partitions
# Format partitions
# Boot EFI
mkfs.vfat -F32 "$BOOTPART"
# Root
mkfs.btrfs -f "$ROOTPART"
# Set up subvolumes
mount "$ROOTPART" /mnt
btrfs subvolume create /mnt/@root
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@tmp
btrfs subvolume create /mnt/@var
btrfs subvolume create /mnt/@snapshots
btrfs subvolume create /mnt/@swap
# Deactivate COW
chattr +C /mnt/@var
chattr +C /mnt/@swap
chattr +C /mnt/@tmp
btrfs subvolume list -t /mnt
umount /mnt
# Mount partitions
# Root
mount -o rw,noatime,ssd,space_cache,commit=120,autodefrag,subvol=@root "$ROOTPART" /mnt
# Create mountpoints
mkdir -p /mnt/boot
mkdir -p /mnt/home
mkdir -p /mnt/tmp
mkdir -p /mnt/var
mkdir -p /mnt/.snapshots
mkdir -p /mnt/.swap
# Rest
mount -o rw,noatime,ssd,space_cache,commit=120,autodefrag,subvol=@home "$ROOTPART" /mnt/home
mount -o rw,noatime,ssd,space_cache,commit=120,subvol=@tmp "$ROOTPART" /mnt/tmp
mount -o rw,noatime,ssd,space_cache,commit=120,subvol=@var "$ROOTPART" /mnt/var
mount -o rw,noatime,ssd,space_cache,commit=120,autodefrag,subvol=@snapshots "$ROOTPART" /mnt/.snapshots
mount -o rw,noatime,ssd,space_cache,commit=120,subvol=@swap "$ROOTPART" /mnt/.swap
# Boot
mount "$BOOTPART" /mnt/boot

# Create swap file
truncate -s 0 /mnt/.swap/swapfile
chattr +C /mnt/.swap/swapfile
btrfs property set /mnt/.swap/swapfile compression none
fallocate -l "$SWAPSIZE" /mnt/.swap/swapfile
chmod 600 /mnt/.swap/swapfile
mkswap /mnt/.swap/swapfile

# * Create mirrorlist
configure_mirrorlist

# * Last check before install
if ! exists arch-chroot; then
    >&2 echo "Meant to be run from the Arch Linux installer"
    exit 1
elif ! grep "$MOUNT " /proc/mounts >/dev/null; then
    >&2 echo "$MOUNT isn't mounted"
    exit 2
fi

# Abort immediately if something goes wrong
set -e

# * Base install
pac base base-devel pacman-contrib linux-firmware linux-zen linux-zen-headers \
    linux-lts linux-lts-headers terminus-font wget git curl ccache bash-completion \
    nano man-db man-pages texinfo logrotate lvm2
# Remove standard kernel
# arch "pacman -Rns linux --noconfirm"
# device mounting drivers
pac gparted
arch "pacman -S $MICRO dosfstools jfsutils f2fs-tools btrfs-progs exfat-utils ntfs-3g \
 reiserfsprogs udftools xfsprogs nilfs-utils polkit gpart mtools --asdeps --needed --noconfirm"

# * Generate fstab
# genfstab -U "$MOUNT" >> "$MOUNT/etc/fstab"
# Add Swap to fstab
# arch "tee -a /etc/fstab << END
# /.swap/swapfile none    swap    defaults    0 0
# END"

rm $MOUNT/etc/fstab 2>/dev/null
# partprobe
lsblk -f
# Get UUIDs for /boot and /
read -rp "UUID for boot: " bootuuid
read -rp "UUID for root: " rootuuid
echo "# Static information about the filesystems." >> $MOUNT/etc/fstab
echo "# See fstab(5) for details." >> $MOUNT/etc/fstab
echo "" >> $MOUNT/etc/fstab
echo "# <file system> <dir> <type> <options> <dump> <pass>" >> $MOUNT/etc/fstab
echo "# Boot" >> $MOUNT/etc/fstab
echo "UUID=$bootuuid      	/boot     	vfat      	rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro	0 2" >> $MOUNT/etc/fstab
echo "" >> $MOUNT/etc/fstab
echo "# Root" >> $MOUNT/etc/fstab
echo "UUID=$rootuuid	/         	btrfs     	rw,noatime,ssd,space_cache,commit=120,autodefrag,subvol=@root	0 0" >> $MOUNT/etc/fstab
echo "" >> $MOUNT/etc/fstab
echo "UUID=$rootuuid	/home     	btrfs     	rw,noatime,ssd,space_cache,commit=120,subvol=@home	0 0" >> $MOUNT/etc/fstab
echo "" >> $MOUNT/etc/fstab
echo "UUID=$rootuuid	/tmp      	btrfs     	rw,noatime,ssd,space_cache,commit=120,subvol=@tmp	0 0" >> $MOUNT/etc/fstab
echo "" >> $MOUNT/etc/fstab
echo "UUID=$rootuuid	/var      	btrfs     	rw,noatime,ssd,space_cache,commit=120,subvol=@var	0 0" >> $MOUNT/etc/fstab
echo "" >> $MOUNT/etc/fstab
echo "UUID=$rootuuid	/.snapshots	btrfs     	rw,noatime,ssd,space_cache,commit=120,autodefrag,subvol=@snapshots	0 0" >> $MOUNT/etc/fstab
echo "" >> $MOUNT/etc/fstab
echo "# Swap" >> $MOUNT/etc/fstab
echo "UUID=$rootuuid	/.swap     	btrfs     	rw,noatime,ssd,space_cache,commit=120,subvol=@swap	0 0" >> $MOUNT/etc/fstab
echo "/.swap/swapfile none    swap    defaults    0 0" >> $MOUNT/etc/fstab

# * Set time zone, language and hosts
arch "ln -sfv /usr/share/zoneinfo/Europe/Berlin /etc/localtime"
arch "hwclock --systohc"
# locale
sed -i '/^#de_DE\.UTF-8/s/^#//' $MOUNT/etc/locale.gen
arch "locale-gen"
echo "LANG=de_DE.UTF-8" > /mnt/etc/locale.conf
echo "FONT=ter-116n" > /mnt/etc/vconsole.conf
# Hostname
echo "$HOSTNAME" > "$MOUNT/etc/hostname"
arch "tee -a /etc/hosts << END
127.0.0.1	localhost
::1		localhost
END"

# * Set up Root and normal user
echo "Please enter root password"
arch "passwd"
# Setup user
read -rp "Please enter user name: " USER_NAME
read -rp "Please enter user groups, e.g. wheel,adm: " USER_GROUPS
arch "useradd -m -G $USER_GROUPS $USER_NAME"
echo "Please enter user password"
arch "passwd $USER_NAME"
# Allow sudo
echo "Uncomment wheel ALL=(ALL) ALL to allow sudo for wheel group"
read -n 1 -s -r -p "Press any key to continue ..."
arch "EDITOR=nano visudo"

# * Set up mkinitcpio
sed -i '/^HOOK/s/keyboard //' $MOUNT/etc/mkinitcpio.conf
if [ "$crypto" == "y" ]; then
    sed -i '/^HOOK/s/autodetect/autodetect keyboard keymap consolefont/' $MOUNT/etc/mkinitcpio.conf
    sed -i '/^HOOK/s/block/block encrypt/' $MOUNT/etc/mkinitcpio.conf
else
    sed -i '/^HOOK/s/autodetect/autodetect keyboard keymap consolefont/' $MOUNT/etc/mkinitcpio.conf
fi

arch "mkinitcpio -P"

# * Install bootloader
arch "bootctl --path=/boot install"

if [ -f $MOUNT/boot/loader/loader.conf ]; then
    rm $MOUNT/boot/loader/loader.conf
fi
if [ -f $MOUNT/boot/loader/entries/arch-zen.conf ]; then
    rm $MOUNT/boot/loader/entries/arch-zen.conf
fi
if [ -f $MOUNT/boot/loader/entries/arch-lts.conf ]; then
    rm $MOUNT/boot/loader/entries/arch-lts.conf
fi

echo "default arch-zen" >> $MOUNT/boot/loader/loader.conf

if [ "$crypto" == "y" ]; then
    lsblk -f
    read -rp "UUID for crypto: " cryptouuid

    echo "title   Arch Linux Zen" >> $MOUNT/boot/loader/entries/arch-zen.conf
    echo "linux   /vmlinuz-linux-zen" >> $MOUNT/boot/loader/entries/arch-zen.conf
    echo "initrd  /$MICRO.img" >> $MOUNT/boot/loader/entries/arch-zen.conf
    echo "initrd  /initramfs-linux-zen.img" >> $MOUNT/boot/loader/entries/arch-zen.conf
    echo "options cryptdevice=UUID=$cryptouuid:crypto root=UUID=$rootuuid rw rootflags=subvol=@root quiet rd.udev.log-priority=3" >> $MOUNT/boot/loader/entries/arch-zen.conf

    echo "title   Arch Linux LTS" >> $MOUNT/boot/loader/entries/arch-lts.conf
    echo "linux   /vmlinuz-linux-lts" >> $MOUNT/boot/loader/entries/arch-lts.conf
    echo "initrd  /$MICRO.img" >> $MOUNT/boot/loader/entries/arch-lts.conf
    echo "initrd  /initramfs-linux-lts.img" >> $MOUNT/boot/loader/entries/arch-lts.conf
    echo "options cryptdevice=UUID=$cryptouuid:crypto root=UUID=$rootuuid rw rootflags=subvol=@root quiet rd.udev.log-priority=3" >> $MOUNT/boot/loader/entries/arch-lts.conf
else
    echo "title   Arch Linux Zen" >> $MOUNT/boot/loader/entries/arch-zen.conf
    echo "linux   /vmlinuz-linux-zen" >> $MOUNT/boot/loader/entries/arch-zen.conf
    echo "initrd  /$MICRO.img" >> $MOUNT/boot/loader/entries/arch-zen.conf
    echo "initrd  /initramfs-linux-zen.img" >> $MOUNT/boot/loader/entries/arch-zen.conf
    echo "options root=UUID=$rootuuid rw rootflags=subvol=@root quiet rd.udev.log-priority=3" >> $MOUNT/boot/loader/entries/arch-zen.conf

    echo "title   Arch Linux LTS" >> $MOUNT/boot/loader/entries/arch-lts.conf
    echo "linux   /vmlinuz-linux-lts" >> $MOUNT/boot/loader/entries/arch-lts.conf
    echo "initrd  /$MICRO.img" >> $MOUNT/boot/loader/entries/arch-lts.conf
    echo "initrd  /initramfs-linux-lts.img" >> $MOUNT/boot/loader/entries/arch-lts.conf
    echo "options root=UUID=$rootuuid rw rootflags=subvol=@root quiet rd.udev.log-priority=3" >> $MOUNT/boot/loader/entries/arch-lts.conf
fi
arch "mkinitcpio -P"

# * Set up makepkg and pacman
sed -i '/^#Color/s/^#//' $MOUNT/etc/pacman.conf

sed -i '/^CFLAGS/s/-march=x86-64 -mtune=generic/-march=native/' $MOUNT/etc/makepkg.conf
sed -i '/^CXXFLAGS/s/-march=x86-64 -mtune=generic -O2 -pipe -fno-plt/\$\{CFLAGS\}/' $MOUNT/etc/makepkg.conf
sed -i '/^#MAKEFLAGS/s/^#//' $MOUNT/etc/makepkg.conf
sed -i '/^MAKEFLAGS/s/-j2/-j\$\(nproc\)/' $MOUNT/etc/makepkg.conf
sed -i '/^BUILDENV/s/!ccache/ccache/' $MOUNT/etc/makepkg.conf
sed -i '/^COMPRESSXZ/s/\(xz -c -z -\)/xz -c -z - --threads=0/' $MOUNT/etc/makepkg.conf

# * Set up yay and needed packages from AUR
# Install Go dep
arch "pacman -S go --asdeps"
#exec command as user instead of root
arch "su - $USER_NAME -c 'git clone https://aur.archlinux.org/yay.git && cd yay && makepkg'"
arch "pacman -Rns go"

# * Download next steps
curl https://gitlab.com/DonHugo/dotfiles-bootstrap/raw/master/bootstrap-Arch-KDE.sh --output bootstrap-Arch-KDE.sh

# * Ending
echo ""
echo "Remember to activate multilib!"
echo ""
echo "Now arch-chroot /mnt"
echo "Navigate to /home/$USER_NAME/yay"
echo "pacman -U yay*"
echo "su - $USER_NAME"
echo "yay -S systemd-boot-pacman-hook pacman-cleanup-hook"
echo ""
echo ""
echo "Time for the next steps!"
echo "Read and run the following order of scripts:"
echo "bootstrap-Arch-KDE.sh"

# Local Variables:
# eval: (outshine-mode 1)
# End:
