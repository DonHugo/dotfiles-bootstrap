#!/usr/bin/env bash

# dotfiles bootstrapper
#
# Installs git, fish & stow, then clones this repo to ~/.dotfiles. This script
# is intended to be run remotely, via curl:
#
# bash <(curl -s https://gitlab.com/DonHugo/dotfiles/raw/master/bootstrap.sh)

export DOTFILES=~/.dotfiles
#set -e

_msg() { printf "\r\033[2K\033[0;32m[ .. ] %s\033[0m\n" "$*"; }
_uncallable() { ! command -v "$1" >/dev/null; }

_msg "Bootstrapping dotfiles"
_msg "You should install your SSH keys before!"
_msg "Git, Fish shell and GNU Stow are required, thus they will be checked first."

# Check if git is installed.
if _uncallable git; then
    _msg "Git not found."
    gitvar=false
else
    _msg "Git found."
    gitvar=true
fi

# Check if fish is installed.
if _uncallable fish; then
    _msg "Fish not found."
    fishvar=false
else
    _msg "Fish found."
    fishvar=true
fi

# Check if stow is installed.
if _uncallable stow; then
    _msg "Stow not found."
    stowvar=false
else
    _msg "Stow found."
    stowvar=true
fi

# Now ask if root is available or if linuxbrew should be used from the beginning.
while true; do
    _msg "Do you have root access?"
    read -p "[y/n] : " rootvar
    case $rootvar in
        [Yy]* )
            rootvar=true
            brewvar=false
            break
            ;;
        [Nn]* )
            rootvar=false
            brewvar=true
            break
            ;;
        * ) echo "Please answer yes or no."
            ;;
    esac
done

if [ $brewvar = false ]; then
    while true; do
        _msg "Do you want to install linuxbrew?"
        read -p "[y/n] : " brewvar
        case $brewvar in
            [Yy]* )
                brewvar=true
                break
                ;;
            [Nn]* )
                brewvar=false
                break
                ;;
            * ) echo "Please answer yes or no."
                ;;
        esac
    done
fi

# Function to install linuxbrew
_install_linuxbrew() {
    _msg "Installing Homebrew/Linuxbrew"
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"

    _msg "Setting PATH in bash"
    test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
    test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
    test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
    echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile

    return 0
}

# Install linuxbrew
if [ $brewvar = true ]; then
    _install_linuxbrew
fi

# Then check if fish is installed. If not,

if [ $gitvar = false ] || [ $fishvar = false ] || [ $stowvar = false ]; then
    _msg "Preparing to install ..."
    if [ $rootvar = true ]; then
        _msg "Installing git and/or fish and stow ..."
        # Detect Arch Linux
        if [[ -f /etc/arch-release ]]; then
            _msg "Arch Linux detected"
            sudo pacman --needed --noconfirm -S git fish stow
        # Detect Arch Linux
        elif [[ -f /etc/solus-release ]]; then
            _msg "Solus Linux detected"
            sudo eopkg install git fish stow
        # Detect Ubuntu
        elif [[ -f /etc/os-release ]]; then
            _msg "Ubuntu detected"
            sudo apt-get update
            # Check if Fish version is current
            apt list fish
            while true; do
                _msg "Please verify that the fish shell version is 3.0 or higher."
                _msg "If not, there might be issues later. In this case, please answer no."
                _msg "Linuxbrew will be used to install a current version of the fish shell."
                read -p "Fish version > 3.0? [y/n]: " fish_version_var
                case $fish_version_var in
                    [Yy]* )
                        sudo apt-get install -y git stow fish
                        break
                        ;;
                    [Nn]* )
                        sudo apt-get install -y git stow
                        _msg "Installing Fish shell"
                        _install_linuxbrew
                        brew install fish
                        break
                        ;;
                    * ) echo "Please answer yes or no."
                        ;;
                esac
            done
        fi
    elif [ $rootvar = false ]; then
        brew install git fish stow
    fi
else
    _msg "Git, Fish shell and stow found, ready to go!"
fi

# Clone repository
if [[ ! -d ~/.dotfiles ]]; then
    _msg "Deploying dotfiles repository..."
    dfrepo=git@gitlab.com:DonHugo/dotfiles.git
    git clone --recursive "$dfrepo" "$DOTFILES"
fi

# Source shell files
test -r ~/.profile && source ~/.profile
test -r ~/.bash_profile && source ~/.bash_profile
test -r ~/.bashrc && source ~/.bashrc

# Initial GNU Stow setup
_msg
_msg "Setting up GNU Stow ..."
_msg

stow -v -d $DOTFILES stow -t ~/

_msg
_msg "And done!"
_msg
_msg "Use $DOTFILES/deploy to install your dotfiles."
